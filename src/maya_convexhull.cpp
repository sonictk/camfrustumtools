#include <cmath>

using std::sqrt;
using std::pow;


double distanceBetween(const MVector &a, const MVector &b)
{
	double result = sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2), + pow((b.z - a.z), 2));
	return result;
}


MVector findCenter(const MVectorArray &pts)
{
	double xMax = 0.0;
	double xMin = 0.0;
	double yMax = 0.0;
	double yMin = 0.0;
	double zMax = 0.0;
	double zMin = 0.0;
	unsigned int numPts = pts.length();
	for (int i=0; i < numPts; ++i) {
		MVector curPt = pts[i];
		if (xMax < curPt.x) {
			xMax = curPt.x;
		}
		if (xMin > curPt.x) {
			xMin = curPt.x;
		}
		if (yMax < curPt.y) {
			yMax = curPt.y;
		}
		if (yMin > curPt.y) {
			yMin = curPt.y;
		}
		if (zMax < curPt.z) {
			zMax = curPt.z;
		}
		if (zMin > curPt.z) {
			zMin = curPt.z;
		}
	}
	MVector centerPt = MVector((xMax - xMin) / 2.0,
							   (yMax - yMin) / 2.0,
							   (zMax - zMin) / 2.0);

	return centerPt;
}


MVectorArray calcConvexHull(const MVectorArray &pts, const MVector &center)
{
	MVectorArray result;

	// TODO: (yliangsiew) Implement

	// TODO: (yliangsiew) First, determine the 4 most extreme points of the mesh.
	// Do this by checking distance from center.
	double xMax = 0.0;
	double xMin = 0.0;
	double yMax = 0.0;
	double yMin = 0.0;
	double zMax = 0.0;
	double zMin = 0.0;
	unsigned int iMinXPt;
	unsigned int iMaxXPt;
	unsigned int numPts = pts.length();
	for (unsigned int i=0; i < numPts; ++i) {
		MVector curPt = pts[i];
		if (xMax < curPt.x) {
			xMax = curPt.x;
		}
		if (xMin > curPt.x) {
			xMin = curPt.x;
		}
		if (yMax < curPt.y) {
			yMax = curPt.y;
		}
		if (yMin > curPt.y) {
			yMin = curPt.y;
		}
		if (zMax < curPt.z) {
			zMax = curPt.z;
		}
		if (zMin > curPt.z) {
			zMin = curPt.z;
		}
	}
	MVector centerPt = MVector((xMax - xMin) / 2.0,
							   (yMax - yMin) / 2.0,
							   (zMax - zMin) / 2.0);
	for (int i=0; i < numPts; ++i) {

	}

	return result;
}
