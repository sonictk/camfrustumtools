#include "mayaCamFrustumTools_gui_cmd.h"


#include <maya/MArgDatabase.h>
#include <maya/MGlobal.h>
#include <maya/MStatus.h>
#include <maya/MSyntax.h>

// NOTE: (Siew Yi Liang) Need to delay Maya MQtUtil imports until all other Qt
// imports have been done in order to prevent compiler errors
#include <maya/MQtUtil.h>


void *ShowCamFrustumToolsGUICmd::creator()
{
	return new ShowCamFrustumToolsGUICmd();
}


MSyntax ShowCamFrustumToolsGUICmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag(MAYACAMFRUSTUMTOOLS_FLAG_HELP_SHORT, MAYACAMFRUSTUMTOOLS_FLAG_HELP);
	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}


MStatus ShowCamFrustumToolsGUICmd::doIt(const MArgList &args)
{
	MStatus result;
	this->setCommandString(MAYACAMFRUSTUMTOOLS_SHOW_TOOLS_CMD_NAME);
	MArgDatabase argDb(syntax(), args, &result);
	CHECK_MSTATUS_AND_RETURN_IT(result);

	if (argDb.isFlagSet(MAYACAMFRUSTUMTOOLS_FLAG_HELP_SHORT)) {
		this->displayInfo(MAYACAMFRUSTUMTOOLS_HELP_TXT);
		return MStatus::kSuccess;

	} else {
		return this->redoIt();
	}
}


MStatus ShowCamFrustumToolsGUICmd::redoIt()
{
	MStatus result;

	QWidget *mayaMainWindow = MQtUtil::mainWindow();
	if (mayaMainWindow == NULL) {
		MGlobal::displayError(MAYACAMFRUSTUMTOOLS_ERR_MSG_NO_MAYA_MAINWINDOW);
		return MStatus::kFailure;
	}

	if (gCamFrustumToolsQWidget == NULL) {
		gCamFrustumToolsQWidget = new CamFrustumToolsQWidget(mayaMainWindow, Qt::Tool);
	}

	QRect mayaMainWindowRect = mayaMainWindow->geometry();
	QPoint mayaMainWindowCenter = mayaMainWindowRect.center();
	gCamFrustumToolsQWidget->setGeometry(mayaMainWindowCenter.x() - (CamFrustumToolsQWidget::defaultWidth / 2),
								   mayaMainWindowCenter.y() - (CamFrustumToolsQWidget::defaultHeight / 2),
								   CamFrustumToolsQWidget::defaultWidth,
								   CamFrustumToolsQWidget::defaultHeight);

	gCamFrustumToolsQWidget->show();
	gCamFrustumToolsQWidget->raise();

	return result;
}


bool ShowCamFrustumToolsGUICmd::isUndoable() const
{
	return false;
}


MStatus ShowCamFrustumToolsGUICmd::undoIt()
{
	return MStatus::kSuccess;
}
