#ifndef MAYA_UTIL_H
#define MAYA_UTIL_H

#include <maya/MObject.h>
#include <maya/MDagPath.h>
#include <maya/MString.h>
#include <maya/MObjectArray.h>
#include <maya/MDagPathArray.h>
#include <maya/MDagPath.h>
#include <maya/MBoundingBox.h>


bool isNodeValid(const MObject &node);


bool isMayaCamera(const MObject &node);


MStatus getMObject(const char *name, MObject &node);

MStatus getMObject(const MString &name, MObject &node);


MStatus getMDagPath(const char *name, MDagPath &dagPath);

MStatus getMDagPath(const MString &name, MDagPath &dagPath);


MObjectArray getAllMayaMeshes();


MDagPathArray getAllMayaMeshesDagPaths();


MDagPathArray getAllMayaDagPathsInScene();


/**
 * Gets the bounding box of ``obj`` in world space.
 *
 * @param obj    The object to calculate the world-space bounding box for.
 *
 * @return       The bounding box.
 */
MBoundingBox calcBBoxOfMObject(const MObject &obj);


MBoundingBox calcBBoxofDagPaths(const MDagPathArray &paths);


MDagPathArray getDagPathsOfMObjects(const MObjectArray &objs);


MBoundingBox calcBBoxOfMObjects(const MObjectArray &objs);


MBoundingBox calcMayaSceneBBox(MDagPathArray &dagPaths);


#endif /* MAYA_UTIL_H */
