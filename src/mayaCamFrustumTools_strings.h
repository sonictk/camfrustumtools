#ifndef MAYACAMFRUSTUMTOOLS_STRINGS_H
#define MAYACAMFRUSTUMTOOLS_STRINGS_H

#define MAYACAMFRUSTUMTOOLS_FLAG_HELP "--help"
#define MAYACAMFRUSTUMTOOLS_FLAG_HELP_SHORT "-h"
#define MAYACAMFRUSTUMTOOLS_SHOW_TOOLS_CMD_NAME "camFrustumTools"
#define MAYACAMFRUSTUMTOOLS_HELP_TXT "Displays the Camera frustum tools."

static const char MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM[] = "selectObjsInFrustum";

static const char MAYACAMFRUSTUMTOOLS_CMD_HELP_FLAG_NAME[] = "-help";
static const char MAYACAMFRUSTUMTOOLS_CMD_HELP_FLAG_SHORT_NAME[] = "-h";

static const char MAYACAMFRUSTUMTOOLS_CMD_CAMNAME_FLAG_NAME[] = "-camera";
static const char MAYACAMFRUSTUMTOOLS_CMD_CAMNAME_FLAG_SHORT_NAME[] = "-cam";

static const char MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_NAME[] = "-anim";
static const char MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_SHORT_NAME[] = "-a";

static const char MAYACAMFRUSTUMTOOLS_CMD_TIME_FLAG_NAME[] = "-time";
static const char MAYACAMFRUSTUMTOOLS_CMD_TIME_FLAG_SHORT_NAME[] = "-t";

static const char MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_HELP_TEXT[] = "Selects the objects that are currently visible within the camera frustum.";

static const char MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_ERR_MSG_INVALID_NAME[] = "You must specify a valid camera name to the command.";
static const char MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_ERR_MSG_INVALID_CAMERA[] = "The camera node specified was invalid.";
static const char MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_ERR_MSG_NO_NODE[] = "No node with the given name was available in the current scene.";

static const char MAYACAMFRUSTUMTOOLS_ERR_MSG_NO_MAYA_MAINWINDOW[] = "There was no available Maya window.";
static const char MAYACAMFRUSTUMTOOLS_ERR_MSG_NO_ACTIVEVIEW[] = "There was no available viewport.";
static const char MAYACAMFRUSTUMTOOLS_ERR_MSG_NO_ACTIVECAM[] = "There was no camera active.";


#endif /* MAYACAMFRUSTUMTOOLS_STRINGS_H */
