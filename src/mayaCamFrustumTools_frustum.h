#ifndef MAYACAMFRUSTUMTOOLS_FRUSTUM_H
#define MAYACAMFRUSTUMTOOLS_FRUSTUM_H

#include <maya/MDagPath.h>
#include <maya/MObjectArray.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MObjectArray.h>
#include <maya/MTime.h>


#define MAYA_CANNOT_GET_ASPECT_RATIO  -50
#define MAYA_CANNOT_GET_RAW_MESH_DATA -60


enum FrustumCheck
{
	FrustumCheck_Outside,
	FrustumCheck_Inside,
	FrustumCheck_Intersect
};


struct MSphere
{
	MVector pos;
	double radius;
};


double signedDistanceBetweenPtAndPlane(const MVector &pt, const MVector &normal, const MVector &planePt);


bool isPtInFrustum(const MVector &pt,
				   const MVector &camPos,
				   const MVector &upVec,
				   const MVector &viewVec,
				   const double vfov,
				   const double aspect,
				   const double near,
				   const double far);


FrustumCheck checkMBBoxWithFrustum(MBoundingBox &bbox,
								   const MVector &camPos,
								   const MVector &upVec,
								   const MVector &viewVec,
								   const double vfov,
								   const double aspect,
								   const double near,
								   const double far);


MSphere getSphereFromBBox(const MBoundingBox &bbox);


FrustumCheck isSphereInFrustum(MSphere &s,
							   const MVector &camPos,
							   const MVector &upVec,
							   const MVector &viewVec,
							   const double hfov,
							   const double vfov,
							   const double near,
							   const double far);


int getMayaObjsInCameraFrustum(const MDagPath &camDagPath, MObjectArray &storage);


int getMayaObjsInAnimCameraFrustum(const MDagPath &camDagPath, MObjectArray &storage, const MTime &start, const MTime &end, const double step);


#endif /* MAYACAMFRUSTUMTOOLS_FRUSTUM_H */
