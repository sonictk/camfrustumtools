#ifndef MAYA_BVH_H
#define MAYA_BVH_H

#include <maya/MObjectArray.h>
#include <maya/MBoundingBox.h>
#include <vector>


#define MAYA_BVH_DEFAULT_MIN_OBJS_PER_LEAF 1


enum MayaBVHStatus
{
	MayaBVHStatus_NoNodes = INT_MIN,
	MayaBVHStatus_Success = 0
};


enum MayaBVHAxis
{
	MayaBVHAxis_Unknown,

	MayaBVHAxis_XY,
	MayaBVHAxis_YZ,
	MayaBVHAxis_XZ
};


struct MayaSceneBVHNode
{
	int childAIdx;
	int childBIdx;
	bool isLeaf;
	MBoundingBox bbox;
	MObjectArray nodes;
};


void initMayaSceneBVHNode(MayaSceneBVHNode &node);


void partitionMObjectsForBVH(const MObjectArray &nodes, const MBoundingBox &bbox, MObjectArray &a, MObjectArray &b);


void constructMayaBVHForMObjects(std::vector<MayaSceneBVHNode>&bvhNodes, const MObjectArray &nodes, const unsigned int minObjsPerLeaf);


MayaBVHStatus constructMayaBVH(std::vector<MayaSceneBVHNode>&bvhNodes, const int minObjsPerLeaf);


#endif /* MAYA_BVH_H */
