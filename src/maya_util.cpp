#include "maya_util.h"

#include <maya/MSelectionList.h>
#include <maya/MObjectHandle.h>
#include <maya/MItDag.h>
#include <maya/MFnDagNode.h>
#include <maya/MMatrix.h>


bool isNodeValid(const MObject &node)
{
	if (!MObjectHandle(node).isValid() || !MObjectHandle(node).isAlive() || node.isNull()) {
		return false;
	}

	return true;
}


bool isMayaCamera(const MObject &node)
{
	if (!isNodeValid(node)) {
		return false;

	} else if (node.hasFn(MFn::kCamera)) {
		return true;
	}

	return false;
}


MStatus getMObject(const char *name, MObject &node)
{
	MSelectionList selList;
	MStatus status = selList.add(name);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	status = selList.getDependNode(0, node);

	return status;
}


MStatus getMObject(const MString &name, MObject &node)
{
	const char *nameC = name.asChar();

	return getMObject(nameC, node);
}


MStatus getMDagPath(const char *name, MDagPath &dagPath)
{
	MSelectionList selList;
	MStatus stat = selList.add(name);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = selList.getDagPath(0, dagPath);

	return stat;
}

MStatus getMDagPath(const MString &name, MDagPath &dagPath)
{
	MSelectionList selList;
	MStatus stat = selList.add(name);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = selList.getDagPath(0, dagPath);

	return stat;
}


MObjectArray getAllMayaMeshes()
{
	MObjectArray result;
	MItDag itDag;
	while (!itDag.isDone()) {
		MObject curNode = itDag.currentItem();

		if (curNode.hasFn(MFn::kMesh)) {
			result.append(curNode);
		}

		itDag.next();
		continue;
	}

	return result;
}


MDagPathArray getAllMayaMeshesDagPaths()
{
	MDagPathArray result;
	MItDag itDag;
	while (!itDag.isDone()) {
		MDagPath curDagPath;
		itDag.getPath(curDagPath);
		if (curDagPath.hasFn(MFn::kMesh)) {
			result.append(curDagPath);
		}

		itDag.next();
		continue;
	}

	return result;
}


MDagPathArray getAllMayaDagPathsInScene()
{
	MDagPathArray result;
	MItDag itDag;
	while (!itDag.isDone()) {
		MDagPath curPath;
		itDag.getPath(curPath);
		result.append(curPath);
		itDag.next();
	}

	return result;
}


MBoundingBox calcBBoxofDagPaths(const MDagPathArray &paths)
{
	MVector sceneBBoxMinPt;
	MVector sceneBBoxMaxPt;

	unsigned int numPaths = paths.length();

	MFnDagNode fnDagNode;
	for (unsigned int i=0; i < numPaths; ++i) {
		MDagPath curDagPath = paths[i];
		MMatrix parentTransformMat = curDagPath.exclusiveMatrix();
		fnDagNode.setObject(curDagPath);
		MBoundingBox curBBox = fnDagNode.boundingBox();
		curBBox.transformUsing(parentTransformMat);

		MPoint curBBoxMin = curBBox.min();
		MPoint curBBoxMax = curBBox.max();

		if (curBBoxMin.x < sceneBBoxMinPt.x) {
			sceneBBoxMinPt.x = curBBoxMin.x;
		}
		if (curBBoxMin.y < sceneBBoxMinPt.y) {
			sceneBBoxMinPt.y = curBBoxMin.y;
		}
		if (curBBoxMin.z < sceneBBoxMinPt.z) {
			sceneBBoxMinPt.z = curBBoxMin.z;
		}

		if (curBBoxMax.x > sceneBBoxMaxPt.x) {
			sceneBBoxMaxPt.x = curBBoxMax.x;
		}
		if (curBBoxMax.y > sceneBBoxMaxPt.y) {
			sceneBBoxMaxPt.y = curBBoxMax.y;
		}
		if (curBBoxMax.z > sceneBBoxMaxPt.z) {
			sceneBBoxMaxPt.z = curBBoxMax.z;
		}
	}

	MBoundingBox result = MBoundingBox(sceneBBoxMinPt, sceneBBoxMaxPt);

	return result;
}


MDagPathArray getDagPathsOfMObjects(const MObjectArray &objs)
{
	MDagPathArray dagPaths;
	MFnDagNode fnDagNode;
	for (unsigned int i=0; i < objs.length(); ++i) {
		MObject obj = objs[i];
		if (!obj.hasFn(MFn::kDagNode)) {
			continue;
		}
		fnDagNode.setObject(objs[i]);
		MDagPath dp;
		fnDagNode.getPath(dp);
		dagPaths.append(dp);
	}

	return dagPaths;
}


MBoundingBox calcBBoxOfMObject(const MObject &obj)
{
	if (!obj.hasFn(MFn::kDagNode)) {
		return MBoundingBox();
	}

	MDagPath dagPath;
	MFnDagNode fnDagNode(obj);
	fnDagNode.getPath(dagPath);

	MVector sceneBBoxMinPt;
	MVector sceneBBoxMaxPt;

	MMatrix parentTransformMat = dagPath.exclusiveMatrix();
	fnDagNode.setObject(dagPath);
	MBoundingBox curBBox = fnDagNode.boundingBox();
	curBBox.transformUsing(parentTransformMat);

	MPoint curBBoxMin = curBBox.min();
	MPoint curBBoxMax = curBBox.max();

	MBoundingBox result = MBoundingBox(curBBoxMin, curBBoxMax);

	return result;
}


MBoundingBox calcBBoxOfMObjects(const MObjectArray &objs)
{
	MBoundingBox result;
	MDagPathArray dagPaths = getDagPathsOfMObjects(objs);
	if (dagPaths.length() == 0) {
		return result;
	}
	result = calcBBoxofDagPaths(dagPaths);

	return result;
}


MBoundingBox calcMayaSceneBBox(MObjectArray &nodes)
{
	MVector sceneBBoxMinPt;
	MVector sceneBBoxMaxPt;

	MItDag itDag;
	MFnDagNode fnDagNode;
	while (!itDag.isDone()) {
		MDagPath curDagPath;
		itDag.getPath(curDagPath);

		MMatrix parentTransformMat = curDagPath.exclusiveMatrix();
		fnDagNode.setObject(curDagPath);
		MBoundingBox curBBox = fnDagNode.boundingBox();
		curBBox.transformUsing(parentTransformMat);

		MPoint curBBoxMin = curBBox.min();
		MPoint curBBoxMax = curBBox.max();

		if (curBBoxMin.x < sceneBBoxMinPt.x) {
			sceneBBoxMinPt.x = curBBoxMin.x;
		}
		if (curBBoxMin.y < sceneBBoxMinPt.y) {
			sceneBBoxMinPt.y = curBBoxMin.y;
		}
		if (curBBoxMin.z < sceneBBoxMinPt.z) {
			sceneBBoxMinPt.z = curBBoxMin.z;
		}

		if (curBBoxMax.x > sceneBBoxMaxPt.x) {
			sceneBBoxMaxPt.x = curBBoxMax.x;
		}
		if (curBBoxMax.y > sceneBBoxMaxPt.y) {
			sceneBBoxMaxPt.y = curBBoxMax.y;
		}
		if (curBBoxMax.z > sceneBBoxMaxPt.z) {
			sceneBBoxMaxPt.z = curBBoxMax.z;

		}

		nodes.append(curDagPath.node());

		itDag.next();
		continue;
	}

	MBoundingBox result = MBoundingBox(sceneBBoxMinPt, sceneBBoxMaxPt);

	return result;
}
