#include "mayaCamFrustumTools_frustum.h"
#include "maya_bvh.h"

#include <cassert>
#include <vector>

#include <maya/MFnCamera.h>
#include <maya/MAnimControl.h>
#include <maya/MItDag.h>
#include <maya/MFnDagNode.h>
#include <maya/MDagPath.h>
#include <maya/MBoundingBox.h>
#include <maya/MTypes.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>


using std::vector;


double signedDistanceBetweenPtAndPlane(const MVector &pt, const MVector &normal, const MVector &planePt)
{
	double result = normal * (pt - planePt);

	return result;
}


bool isPtInFrustum(const MVector &pt,
				   const MVector &camPos,
				   const MVector &upVec,
				   const MVector &viewVec,
				   const double vfov,
				   const double aspect,
				   const double near,
				   const double far)
{
	// NOTE: (sonictk) Using "radar" approach as described in Game Programming Gems 5.
	// Vector from cam pos to Pt
	MVector v = pt - camPos;
	// First test z-coordinate, then y, and finally x.
	double pcz = v * viewVec;
	if (pcz < near || pcz > far) {
		return false;
	}

	// Height of view frustum at distance pcz from the camera.
	double h = pcz * 2.0 * tan(vfov / 2.0);
	double half_h = h / 2.0;
	double pcy = v * upVec;
	if (pcy < -half_h || pcy > half_h) {
		return false;
	}

	double width = h * aspect;

	MVector camX = upVec ^ viewVec;
	double pcx = v * camX;
	double width_half = width / 2.0;
	if (pcx < -width_half || pcx > width_half) {
		return false;
	}

	return true;
}


FrustumCheck checkMBBoxWithFrustum(MBoundingBox &bbox,
								   const MVector &camPos,
								   const MVector &upVec,
								   const MVector &viewVec,
								   const double vfov,
								   const double aspect,
								   const double near,
								   const double far)
{
	MPoint minPt = bbox.min();
	MPoint maxPt = bbox.max();

	FrustumCheck result = FrustumCheck_Inside;

	// NOTE: (sonictk) For each plane, check the p and n vertices and determine
	// frustum outside/intersection/inside.
	// But first, we need to figure out the 6 planes and their normals.
	double vfov_factor = tan(vfov / 2.0);
	double heightNearPlane =  2.0 * vfov_factor * near;
	double widthNearPlane = aspect * heightNearPlane;

	double heightFarPlane = 2.0 * vfov_factor * far;
	double widthFarPlane = aspect * heightFarPlane;

	MVector camRightVec = viewVec ^ upVec;

	MVector nearPlaneCenterPt = camPos + (viewVec * near);
	MVector nearPlaneTopLf = nearPlaneCenterPt + (upVec * heightNearPlane / 2.0) - (camRightVec * widthNearPlane / 2.0);
	MVector nearPlaneTopRt = nearPlaneCenterPt + (upVec * heightNearPlane / 2.0) + (camRightVec * widthNearPlane / 2.0);
	MVector nearPlaneBotLf = nearPlaneCenterPt - (upVec * heightNearPlane / 2.0) - (camRightVec * widthNearPlane / 2.0);
	MVector nearPlaneBotRt = nearPlaneCenterPt - (upVec * heightNearPlane / 2.0) + (camRightVec * widthNearPlane / 2.0);

	MVector farPlaneCenterPt = camPos + (viewVec * far);
	MVector farPlaneTopLf = farPlaneCenterPt + (upVec * heightFarPlane / 2.0) - (camRightVec * widthFarPlane / 2.0);
	MVector farPlaneTopRt = farPlaneCenterPt + (upVec * heightFarPlane / 2.0) + (camRightVec * widthFarPlane / 2.0);
	MVector farPlaneBotLf = farPlaneCenterPt - (upVec * heightFarPlane / 2.0) - (camRightVec * widthFarPlane / 2.0);
	MVector farPlaneBotRt = farPlaneCenterPt - (upVec * heightFarPlane / 2.0) + (camRightVec * widthFarPlane / 2.0);

	// Near plane positive/negative vertex determination
	MVector pVtx = MVector(minPt.x, minPt.y, minPt.z);
	if (viewVec.x >= 0) {
		pVtx.x = maxPt.x;
	}
	if (viewVec.y >= 0) {
		pVtx.y = maxPt.y;
	}
	if (viewVec.z >= 0) {
		pVtx.z = maxPt.z;
	}
	// If any of the positive vertices are on the wrong side of any plane, the bbox _has_
	// to be outside of the frustum.
	if (signedDistanceBetweenPtAndPlane(pVtx, viewVec, nearPlaneCenterPt) < 0) {
		return FrustumCheck_Outside;
	}

	MVector nVtx = MVector(maxPt.x, maxPt.y, maxPt.z);
	if (viewVec.x >= 0) {
		nVtx.x = minPt.x;
	}
	if (viewVec.y >= 0) {
		nVtx.y = minPt.y;
	}
	if (viewVec.z >= 0) {
		nVtx.z = minPt.z;
	}
	// Means we potentially at least have an intersection with _at least_ one plane.
	if (signedDistanceBetweenPtAndPlane(nVtx, viewVec, nearPlaneCenterPt) < 0) {
		result = FrustumCheck_Intersect;
	}

	// Far plane; basically just copy paste the code for other planes; just swap in the
	// code for the plane points and normals.
	pVtx = MVector(minPt.x, minPt.y, minPt.z);
	if (-viewVec.x >= 0) {
		pVtx.x = maxPt.x;
	}
	if (-viewVec.y >= 0) {
		pVtx.y = maxPt.y;
	}
	if (-viewVec.z >= 0) {
		pVtx.z = maxPt.z;
	}
	if (signedDistanceBetweenPtAndPlane(pVtx, -viewVec, farPlaneCenterPt) < 0) {
		return FrustumCheck_Outside;
	}
	nVtx = MVector(maxPt.x, maxPt.y, maxPt.z);
	if (-viewVec.x >= 0) {
		nVtx.x = minPt.x;
	}
	if (-viewVec.y >= 0) {
		nVtx.y = minPt.y;
	}
	if (-viewVec.z >= 0) {
		nVtx.z = minPt.z;
	}
	// Means we potentially at least have an intersection with _at least_ one plane.
	if (signedDistanceBetweenPtAndPlane(nVtx, -viewVec, farPlaneCenterPt) < 0) {
		result = FrustumCheck_Intersect;
	}

	// Top plane ; camera position will be a point on top/bot/lf/rt planes for sure,
	// assuming perspective projection.
	MVector viewToNearTop = (nearPlaneCenterPt + (upVec * heightNearPlane / 2.0)) - camPos;
	viewToNearTop.normalize();
	MVector topPlaneNormal = viewToNearTop ^ camRightVec;

	pVtx = MVector(minPt.x, minPt.y, minPt.z);
	if (topPlaneNormal.x >= 0) {
		pVtx.x = maxPt.x;
	}
	if (topPlaneNormal.y >= 0) {
		pVtx.y = maxPt.y;
	}
	if (topPlaneNormal.z >= 0) {
		pVtx.z = maxPt.z;
	}
	if (signedDistanceBetweenPtAndPlane(pVtx, topPlaneNormal, camPos) < 0) {
		return FrustumCheck_Outside;
	}
	nVtx = MVector(maxPt.x, maxPt.y, maxPt.z);
	if (topPlaneNormal.x >= 0) {
		nVtx.x = minPt.x;
	}
	if (topPlaneNormal.y >= 0) {
		nVtx.y = minPt.y;
	}
	if (topPlaneNormal.z >= 0) {
		nVtx.z = minPt.z;
	}
	// Means we potentially at least have an intersection with _at least_ one plane.
	if (signedDistanceBetweenPtAndPlane(nVtx, topPlaneNormal, camPos) < 0) {
		result = FrustumCheck_Intersect;
	}

	// Bottom plane
	MVector viewToNearBot = (nearPlaneCenterPt - (upVec * heightNearPlane / 2.0)) - camPos;
	viewToNearBot.normalize();
	MVector botPlaneNormal = viewToNearBot ^ -camRightVec;

	pVtx = MVector(minPt.x, minPt.y, minPt.z);
	if (botPlaneNormal.x >= 0) {
		pVtx.x = maxPt.x;
	}
	if (botPlaneNormal.y >= 0) {
		pVtx.y = maxPt.y;
	}
	if (botPlaneNormal.z >= 0) {
		pVtx.z = maxPt.z;
	}
	if (signedDistanceBetweenPtAndPlane(pVtx, botPlaneNormal, camPos) < 0) {
		return FrustumCheck_Outside;
	}
	nVtx = MVector(maxPt.x, maxPt.y, maxPt.z);
	if (botPlaneNormal.x >= 0) {
		nVtx.x = minPt.x;
	}
	if (botPlaneNormal.y >= 0) {
		nVtx.y = minPt.y;
	}
	if (botPlaneNormal.z >= 0) {
		nVtx.z = minPt.z;
	}
	// Means we potentially at least have an intersection with _at least_ one plane.
	if (signedDistanceBetweenPtAndPlane(nVtx, botPlaneNormal, camPos) < 0) {
		result = FrustumCheck_Intersect;
	}

	// Left plane
	MVector viewToNearLeft = (nearPlaneCenterPt - (camRightVec * widthNearPlane / 2.0)) - camPos;
	viewToNearLeft.normalize();
	MVector lfPlaneNormal = viewToNearLeft ^ upVec;

	pVtx = MVector(minPt.x, minPt.y, minPt.z);
	if (lfPlaneNormal.x >= 0) {
		pVtx.x = maxPt.x;
	}
	if (lfPlaneNormal.y >= 0) {
		pVtx.y = maxPt.y;
	}
	if (lfPlaneNormal.z >= 0) {
		pVtx.z = maxPt.z;
	}
	if (signedDistanceBetweenPtAndPlane(pVtx, lfPlaneNormal, camPos) < 0) {
		return FrustumCheck_Outside;
	}
	nVtx = MVector(maxPt.x, maxPt.y, maxPt.z);
	if (lfPlaneNormal.x >= 0) {
		nVtx.x = minPt.x;
	}
	if (lfPlaneNormal.y >= 0) {
		nVtx.y = minPt.y;
	}
	if (lfPlaneNormal.z >= 0) {
		nVtx.z = minPt.z;
	}
	// Means we potentially at least have an intersection with _at least_ one plane.
	if (signedDistanceBetweenPtAndPlane(nVtx, lfPlaneNormal, camPos) < 0) {
		result = FrustumCheck_Intersect;
	}

	// Right plane
	MVector viewToNearRt = (nearPlaneCenterPt + (camRightVec * widthNearPlane / 2.0)) - camPos;
	viewToNearLeft.normalize();
	MVector rtPlaneNormal = viewToNearRt ^ -upVec;

	pVtx = MVector(minPt.x, minPt.y, minPt.z);
	if (rtPlaneNormal.x >= 0) {
		pVtx.x = maxPt.x;
	}
	if (rtPlaneNormal.y >= 0) {
		pVtx.y = maxPt.y;
	}
	if (rtPlaneNormal.z >= 0) {
		pVtx.z = maxPt.z;
	}
	if (signedDistanceBetweenPtAndPlane(pVtx, rtPlaneNormal, camPos) < 0) {
		return FrustumCheck_Outside;
	}
	nVtx = MVector(maxPt.x, maxPt.y, maxPt.z);
	if (rtPlaneNormal.x >= 0) {
		nVtx.x = minPt.x;
	}
	if (rtPlaneNormal.y >= 0) {
		nVtx.y = minPt.y;
	}
	if (rtPlaneNormal.z >= 0) {
		nVtx.z = minPt.z;
	}
	// Means we potentially at least have an intersection with _at least_ one plane.
	if (signedDistanceBetweenPtAndPlane(nVtx, rtPlaneNormal, camPos) < 0) {
		result = FrustumCheck_Intersect;
	}

	return result;
}


MSphere getSphereFromBBox(const MBoundingBox &bbox)
{
	double width = bbox.width();
	double height = bbox.height();
	double depth = bbox.depth();
	double r = width ? width > height : height;
	r = depth ? depth > r : r;

	MSphere result;
	result.pos = bbox.center();
	result.radius = r;

	return result;
}


FrustumCheck isSphereInFrustum(MSphere &s,
							   const MVector &camPos,
							   const MVector &upVec,
							   const MVector &viewVec,
							   const double hfov,
							   const double vfov,
							   const double near,
							   const double far)
{
	// NOTE: (sonictk) This uses the radar approach as described in Game Programming Gems 5.
	FrustumCheck result = FrustumCheck_Inside;
	MVector v = s.pos - camPos; // Vector from camera to sphere center
	double pcz = v * viewVec; // Projection from above vector onto view direction vector
	if (pcz > far + s.radius || pcz < near - s.radius) {
		return FrustumCheck_Outside;
	}
	if (pcz > far - s.radius || pcz < near + s.radius) {
		result = FrustumCheck_Intersect;
	}
	// Height of view frustum at distance pcz from the camera.
	double h = pcz * 2.0 * tan(vfov / 2.0);
	double half_h = h / 2.0;
	double dy = s.radius / cos(vfov); // Distance from center of sphere to top plane along near/far frustum plane.
	if (dy > half_h + s.radius || dy < half_h - s.radius) {
		return FrustumCheck_Outside;
	}
	if (dy > half_h - s.radius || dy < half_h + s.radius) {
		result = FrustumCheck_Intersect;
	}

	MVector camX = viewVec ^ upVec;
	double dx = s.radius / cos(hfov);
	if (dx > half_h + s.radius || dx < half_h - s.radius) {
		return FrustumCheck_Outside;
	}
	if (dx > half_h - s.radius || dx < half_h + s.radius) {
		result = FrustumCheck_Intersect;
	}

	return result;
}


int getMayaObjsInCameraFrustum(const MDagPath &camDagPath, MObjectArray &storage)
{
	assert(camDagPath.hasFn(MFn::kCamera));

	int numOfObjectsInFrustum = 0;

	MFnCamera fnCam(camDagPath);
	MStatus result;
	double aspect = fnCam.aspectRatio(&result);
	CHECK_MSTATUS_AND_RETURN(result, MAYA_CANNOT_GET_ASPECT_RATIO);

	double camNearClip = fnCam.nearClippingPlane();
	double camFarClip = fnCam.farClippingPlane();

	MPoint camPt = fnCam.eyePoint(MSpace::kWorld);
	MVector camPos = MVector(camPt.x, camPt.y, camPt.z);

	MVector camUpVec = fnCam.upDirection(MSpace::kWorld);
	MVector camViewVec = fnCam.viewDirection(MSpace::kWorld);
	double vfov = fnCam.verticalFieldOfView();

	MFnDagNode fnDagNode;
	MFnMesh fnMesh;
	MItDag itDag;
	while (!itDag.isDone()) {
		MObject curItem = itDag.currentItem();
		fnDagNode.setObject(curItem);
		MDagPath curItemDagPath;
		fnDagNode.getPath(curItemDagPath);
		MMatrix parentTransformMat = curItemDagPath.exclusiveMatrix();
		MBoundingBox bbox = fnDagNode.boundingBox();
		bbox.transformUsing(parentTransformMat);
		FrustumCheck intersect = checkMBBoxWithFrustum(bbox,
													   camPos,
													   camUpVec,
													   camViewVec,
													   vfov,
													   aspect,
													   camNearClip,
													   camFarClip);
		if (intersect == FrustumCheck_Outside) {
			itDag.next();
			continue;
		}

		// TODO: (sonictk) Now construct the convex hull and test against it for
		// intersection if it is a mesh. If the convex hull test is true, then check all vertices.
		// Maybe don't construct convex hull unless a certain number of vertices in terms of threshold is hit.
		if (curItem.hasFn(MFn::kMesh)) {
			fnMesh.setObject(curItemDagPath);
			int numVerts = fnMesh.numVertices();
			const float *meshPts = fnMesh.getRawPoints(&result);
			if (result != MStatus::kSuccess) {
				// TODO: (yliangsiew) Should probably fallback to something if mesh points cannot be retrieved
				itDag.next();
				continue;
			}
			static const int sizeOfVertexData = 3 * sizeof(float);
			int meshPtsLen = sizeOfVertexData * numVerts * sizeof(uint8_t);
			int offset = 0;
			while (offset < meshPtsLen) {
				MVector meshPt = MVector(meshPts[offset],
										 meshPts[offset + sizeOfVertexData],
										 meshPts[offset + (sizeOfVertexData * 2)]);
				bool checkInFrustum = isPtInFrustum(meshPt,
													camPos,
													camUpVec,
													camViewVec,
													vfov,
													aspect,
													camNearClip,
													camFarClip);
				if (checkInFrustum) {
					break;
				}

				offset += (sizeOfVertexData * 2);
			}
			// NOTE: (yliangsiew) Means that we checked all vertices and they weren't in frustum.
			itDag.next();
			continue;
		}

		storage.append(curItem);
		itDag.next();
	}

	return numOfObjectsInFrustum;
}


int getMayaObjsInAnimCameraFrustum(const MDagPath &camDagPath, MObjectArray &storage, const MTime &start, const MTime &end, const double step)
{
	assert(camDagPath.hasFn(MFn::kCamera));

	MStatus result;

	int numOfObjectsInFrustum = 0;

	MFnCamera fnCam(camDagPath);

	MTime::Unit currentUnit = start.unit();
	double startTimeVal = start.value();
	double endTimeVal = end.value();

	MTime origTime = MAnimControl::currentTime();

	// TODO: (yliangsiew) This is where optimizations can come with with BVH search,
	// swept volumes etc.
	MObjectArray allMeshes = getAllMayaMeshes();
	// vector<MayaSceneBVHNode> bvhNodes;
	// constructMayaBVHForMObjects(bvhNodes, allMeshes, MAYA_BVH_DEFAULT_MIN_OBJS_PER_LEAF);

	unsigned int numObjs = allMeshes.length();
	if (numObjs == 0) {
		return 0;
	}
	for (double i=startTimeVal; i < endTimeVal; i+=step) {
		if (numObjs == 0) {
			break;
		}

		MTime iTime = MTime(i, currentUnit);
		MAnimControl::setCurrentTime(iTime);

		double aspect = fnCam.aspectRatio(&result);
		CHECK_MSTATUS_AND_RETURN_IT(result);

		double camNearClip = fnCam.nearClippingPlane();
		double camFarClip = fnCam.farClippingPlane();

		MPoint camPt = fnCam.eyePoint(MSpace::kWorld);
		MVector camPos = MVector(camPt.x, camPt.y, camPt.z);

		MVector camUpVec = fnCam.upDirection(MSpace::kWorld);
		MVector camViewVec = fnCam.viewDirection(MSpace::kWorld);
		double vfov = fnCam.verticalFieldOfView();

		MFnDagNode fnDagNode;

		for (unsigned int j=numObjs; j > 0; --j) {
			MObject curItem = allMeshes[j - 1];
			fnDagNode.setObject(curItem);
			MDagPath curItemDagPath;
			fnDagNode.getPath(curItemDagPath);
			MMatrix parentTransformMat = curItemDagPath.exclusiveMatrix();
			MBoundingBox bbox = fnDagNode.boundingBox();
			bbox.transformUsing(parentTransformMat);
			FrustumCheck intersect = checkMBBoxWithFrustum(bbox,
														   camPos,
														   camUpVec,
														   camViewVec,
														   vfov,
														   aspect,
														   camNearClip,
														   camFarClip);
			if (intersect == FrustumCheck_Outside) {
				continue;
			}

			MString curItemName = curItemDagPath.fullPathName();
			char tmp[256];
			sprintf(tmp, "%f", iTime.value());

			MGlobal::displayInfo(curItemName);
			MGlobal::displayInfo(tmp);


			// TODO: (sonictk) Now construct the convex hull and test against it for
			// intersection.
			allMeshes.remove(j - 1);
			--numObjs;
			storage.append(curItem);
		}
	}

	MAnimControl::setCurrentTime(origTime);

	return numOfObjectsInFrustum;
}
