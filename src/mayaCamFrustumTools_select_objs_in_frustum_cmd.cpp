#include "mayaCamFrustumTools_select_objs_in_frustum_cmd.h"
#include <maya/MAnimControl.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MGlobal.h>
#include <maya/MObjectArray.h>


void *SelectObjsInMayaCamFrustumCmd::creator()
{
	SelectObjsInMayaCamFrustumCmd *cmd = new SelectObjsInMayaCamFrustumCmd();

	cmd->flagHelpSpecified = false;
	cmd->captureTimeSpecified = false;
	cmd->animFrustum = false;
	cmd->step = 0.0;
	cmd->captureTime = MAnimControl::currentTime();
	cmd->startTime = MAnimControl::minTime();
	cmd->startTimeValue = cmd->startTime.value();
	cmd->endTime = MAnimControl::maxTime();
	cmd->endTimeValue = cmd->endTime.value();
	cmd->cameraNode = MObject::kNullObj;
	cmd->cameraNodeName = "";

	return cmd;
}


MSyntax SelectObjsInMayaCamFrustumCmd::newSyntax()
{
	MSyntax syntax;

	syntax.setObjectType(MSyntax::kSelectionList);
	syntax.enableQuery(false);
	syntax.enableEdit(false);
	syntax.useSelectionAsDefault(true);

	syntax.addFlag(MAYACAMFRUSTUMTOOLS_CMD_HELP_FLAG_SHORT_NAME,
				   MAYACAMFRUSTUMTOOLS_CMD_HELP_FLAG_NAME);

	syntax.addFlag(MAYACAMFRUSTUMTOOLS_CMD_CAMNAME_FLAG_SHORT_NAME,
				   MAYACAMFRUSTUMTOOLS_CMD_CAMNAME_FLAG_NAME,
				   MSyntax::kString);

	syntax.addFlag(MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_SHORT_NAME,
				   MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_NAME,
				   MSyntax::kBoolean,
				   MSyntax::kDouble,
				   MSyntax::kDouble,
				   MSyntax::kDouble);

	syntax.addFlag(MAYACAMFRUSTUMTOOLS_CMD_TIME_FLAG_SHORT_NAME,
				   MAYACAMFRUSTUMTOOLS_CMD_TIME_FLAG_NAME,
				   MSyntax::kBoolean,
				   MSyntax::kDouble);

	return syntax;
}


MStatus SelectObjsInMayaCamFrustumCmd::parseArgs(const MArgList &args)
{
	MStatus result;

	MArgDatabase argDb(this->syntax(), args, &result);
	CHECK_MSTATUS_AND_RETURN_IT(result);

	if (argDb.isFlagSet(MAYACAMFRUSTUMTOOLS_CMD_HELP_FLAG_NAME)) {
		MGlobal::displayInfo(MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_HELP_TEXT);
		this->flagHelpSpecified = true;

		return MStatus::kSuccess;
	}
	else {
		this->flagHelpSpecified = false;
	}

	if (argDb.isFlagSet(MAYACAMFRUSTUMTOOLS_CMD_CAMNAME_FLAG_NAME)) {
		result = argDb.getFlagArgument(MAYACAMFRUSTUMTOOLS_CMD_CAMNAME_FLAG_NAME, 0, this->cameraNodeName);
		CHECK_MSTATUS_AND_RETURN_IT(result);
	}

	if (argDb.isFlagSet(MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_NAME)) {
		result = argDb.getFlagArgument(MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_NAME, 0, this->animFrustum);
		CHECK_MSTATUS_AND_RETURN_IT(result);
		result = argDb.getFlagArgument(MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_NAME, 1, this->startTimeValue);
		CHECK_MSTATUS_AND_RETURN_IT(result);
		result = argDb.getFlagArgument(MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_NAME, 2, this->endTimeValue);
		CHECK_MSTATUS_AND_RETURN_IT(result);
		result = argDb.getFlagArgument(MAYACAMFRUSTUMTOOLS_CMD_ANIM_FLAG_NAME, 3, this->step);
		CHECK_MSTATUS_AND_RETURN_IT(result);
	} else if (argDb.isFlagSet(MAYACAMFRUSTUMTOOLS_CMD_TIME_FLAG_NAME)) {
		result = argDb.getFlagArgument(MAYACAMFRUSTUMTOOLS_CMD_TIME_FLAG_NAME, 0, this->captureTimeSpecified);
		CHECK_MSTATUS_AND_RETURN_IT(result);
		result = argDb.getFlagArgument(MAYACAMFRUSTUMTOOLS_CMD_TIME_FLAG_NAME, 1, this->captureTimeValue);
		this->captureTimeSpecified = true;
	}

	return result;
}


MStatus SelectObjsInMayaCamFrustumCmd::redoIt()
{
	MStatus result;

	result = getMObject(this->cameraNodeName, this->cameraNode);
	if (result != MStatus::kSuccess) {
		MGlobal::displayError(MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_ERR_MSG_NO_NODE);
		return MStatus::kInvalidParameter;
	}
	if (!isMayaCamera(this->cameraNode)) {
		MGlobal::displayError(MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_ERR_MSG_INVALID_CAMERA);
		return MStatus::kInvalidParameter;
	}

	MDagPath camDagPath;
	result = getMDagPath(this->cameraNodeName, camDagPath);
	CHECK_MSTATUS_AND_RETURN(result, MStatus::kInvalidParameter);

	MObjectArray objsToSelect;

	if (captureTimeSpecified) {
		MTime::Unit currentUIUnit = MTime::uiUnit();
		this->captureTime = MTime(captureTimeValue, currentUIUnit);
		MAnimControl::setCurrentTime(this->captureTime);
	} else if (animFrustum) {
		MTime::Unit currentUIUnit = MTime::uiUnit();
		this->startTime = MTime(startTimeValue, currentUIUnit);
		this->endTime = MTime(endTimeValue, currentUIUnit);
		getMayaObjsInAnimCameraFrustum(camDagPath, objsToSelect, startTime, endTime, step);
	} else {
		getMayaObjsInCameraFrustum(camDagPath, objsToSelect);
	}

	MSelectionList selList;
	for (unsigned int i=0; i < objsToSelect.length(); ++i) {
		selList.add(objsToSelect[i]);
	}
	MGlobal::selectCommand(selList);

	return result;
}


MStatus SelectObjsInMayaCamFrustumCmd::doIt(const MArgList &args)
{
	this->clearResult();
	this->setCommandString(MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM);

	MStatus stat = this->parseArgs(args);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	if (this->flagHelpSpecified == true) {
		return MStatus::kSuccess;
	}

	if (this->cameraNodeName.numChars() == 0) {
		MGlobal::displayError(MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM_ERR_MSG_INVALID_NAME);
		return MStatus::kInvalidParameter;
	}

	return this->redoIt();
}


MStatus SelectObjsInMayaCamFrustumCmd::undoIt()
{
	return MStatus::kSuccess;
}


bool SelectObjsInMayaCamFrustumCmd::isUndoable() const
{
	return false;
}
