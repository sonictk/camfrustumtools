#include "mayaCamFrustumTools_strings.h"

#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MObject.h>
#include <maya/MStatus.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>

#include "maya_util.cpp"
#include "maya_bvh.cpp"
#include "mayaCamFrustumTools_frustum.cpp"
#include "mayaCamFrustumTools_select_objs_in_frustum_cmd.cpp"
#include "mayaCamFrustumTools_qt_gui.cpp"
#include "mayaCamFrustumTools_gui_cmd.cpp"


static const char MAYACAMFRUSTUMTOOLS_AUTHOR[] = "Siew Yi Liang";
static const char MAYACAMFRUSTUMTOOLS_VERSION[] = "1.0.0";


/**
 * This is the entry point for the plugin.
 *
 * @param obj	The internal Maya object containing Maya private information
 * 			about the plug-in.
 *
 * @return		Status code.
 */
MStatus initializePlugin(MObject obj)
{
	// NOTE: (sonictk) Version here should be synced with the changelog in ``readme.md``.
	MFnPlugin plugin(obj, MAYACAMFRUSTUMTOOLS_AUTHOR, MAYACAMFRUSTUMTOOLS_VERSION);
	MStatus result;

	result = plugin.registerCommand(MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM,
									SelectObjsInMayaCamFrustumCmd::creator,
									SelectObjsInMayaCamFrustumCmd::newSyntax);
	CHECK_MSTATUS_AND_RETURN_IT(result);

	result = plugin.registerCommand(MAYACAMFRUSTUMTOOLS_SHOW_TOOLS_CMD_NAME,
									ShowCamFrustumToolsGUICmd::creator,
									ShowCamFrustumToolsGUICmd::newSyntax);

	return result;
}


/**
 * This function is called when the plugin is de-registered in Maya. It is the
 * exit point of the plugin.
 *
 * @param obj	The internal Maya object containing Maya private information
 * 			about the plug-in.
 *
 * @return 	Status code.
 */
MStatus uninitializePlugin(MObject obj)
{
	if (gCamFrustumToolsQWidget != NULL) {
		gCamFrustumToolsQWidget->close();
		gCamFrustumToolsQWidget->deleteLater();
		delete gCamFrustumToolsQWidget;
		gCamFrustumToolsQWidget = NULL;
	}

	MStatus result;
	MFnPlugin plugin(obj);

	result = plugin.deregisterCommand(MAYACAMFRUSTUMTOOLS_CMD_SELECT_OBJECTS_IN_CAM_FRUSTUM);
	CHECK_MSTATUS_AND_RETURN_IT(result);
	result = plugin.deregisterCommand(MAYACAMFRUSTUMTOOLS_SHOW_TOOLS_CMD_NAME);

	return result;
}
