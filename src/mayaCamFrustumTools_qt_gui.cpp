#include "mayaCamFrustumTools_qt_gui.h"

#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MObjectArray.h>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>

#include <QtWidgets/QGroupBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>


static CamFrustumToolsQWidget *gCamFrustumToolsQWidget = NULL;


const char *CamFrustumToolsQWidget::defaultObjName = "camFrustumToolsQWidget";
const char *CamFrustumToolsQWidget::defaultWindowTitle = "Camera frustum tools";
const int CamFrustumToolsQWidget::defaultWidth = 250;
const int CamFrustumToolsQWidget::defaultHeight = 200;

static const char CAMFRUSTUMTOOLS_HEADER_TXT[] = "Frustum tools";
static const char CAMFRUSTUMTOOLS_LBL_SELECT_TXT[] = "Select visible object(s)";


CamFrustumToolsQWidget::CamFrustumToolsQWidget(QWidget *parent, Qt::WindowFlags flags) : QWidget(parent, flags)
{
	this->setWindowTitle(CamFrustumToolsQWidget::defaultWindowTitle);
	QGroupBox *grpBoxFrustumTools = new QGroupBox(CAMFRUSTUMTOOLS_HEADER_TXT, this);
	QVBoxLayout *lytFrustumTools = new QVBoxLayout();

	QPushButton *btnSelectObjsInFrustum = new QPushButton(CAMFRUSTUMTOOLS_LBL_SELECT_TXT, this);
	lytFrustumTools->addWidget(btnSelectObjsInFrustum);

	grpBoxFrustumTools->setLayout(lytFrustumTools);

	QVBoxLayout *lytMain = new QVBoxLayout();
	lytMain->addWidget(grpBoxFrustumTools);
	lytMain->addStretch();
	this->setLayout(lytMain);

	this->connect(btnSelectObjsInFrustum, &QPushButton::clicked,
				  this, &CamFrustumToolsQWidget::selectObjsInCurrentCamFrustumCB);

	return;
}


CamFrustumToolsQWidget::~CamFrustumToolsQWidget() {}


void CamFrustumToolsQWidget::selectObjsInCurrentCamFrustumCB()
{
	MStatus stat;
	M3dView activeView = M3dView::active3dView(&stat);
	if (stat != MStatus::kSuccess) {
		MGlobal::displayError(MAYACAMFRUSTUMTOOLS_ERR_MSG_NO_ACTIVEVIEW);
		return;
	}

	MDagPath curCam;
	stat = activeView.getCamera(curCam);
	if (stat != MStatus::kSuccess) {
		MGlobal::displayError(MAYACAMFRUSTUMTOOLS_ERR_MSG_NO_ACTIVECAM);
		return;
	}

	MObjectArray objsToSelect;
	getMayaObjsInCameraFrustum(curCam, objsToSelect);

	MSelectionList selList;
	for (unsigned int i=0; i < objsToSelect.length(); ++i) {
		selList.add(objsToSelect[i]);
	}
	MGlobal::selectCommand(selList);

	return;
}
