#ifndef MAYACAMFRUSTUMTOOLS_QT_GUI_H
#define MAYACAMFRUSTUMTOOLS_QT_GUI_H

#include <QtCore/QObject>
#include <QtCore/Qt>
#include <QtWidgets/QWidget>


class CamFrustumToolsQWidget : public QWidget
{
	Q_OBJECT

public:
	CamFrustumToolsQWidget(QWidget *parent, Qt::WindowFlags flags);
	virtual ~CamFrustumToolsQWidget();

	static const int defaultWidth;
	static const int defaultHeight;

	static const char *defaultWindowTitle;
	static const char *defaultObjName;

private slots:
	void selectObjsInCurrentCamFrustumCB();
};


#endif /* MAYACAMFRUSTUMTOOLS_QT_GUI_H */
