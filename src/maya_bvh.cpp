#include "maya_bvh.h"

#include <cassert>
#include <cstdio>

#include <maya/MDagPath.h>
#include <maya/MItDag.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnTransform.h>
#include <maya/MFnMesh.h>
#include <maya/MObject.h>
#include <maya/MMatrix.h>

using std::malloc;
using std::vector;


void initMayaSceneBVHNode(MayaSceneBVHNode &node)
{
	node.childAIdx = -1;
	node.childBIdx = -1;
	node.isLeaf = true;
	node.bbox = MBoundingBox();
	node.nodes.clear();

	return;
}


void partitionMObjectsForBVH(const MObjectArray &nodes, const MBoundingBox &bbox, MObjectArray &a, MObjectArray &b)
{
	unsigned int numNodes = nodes.length();
	if (numNodes <= 1) {
		return;
	}

	// TODO: (sonictk) This is a bad idea, because what happens if you have two identical
	// objects in the same position? How do you partition those?

	// NOTE: (sonictk) Find axis of largest variance for all the nodes and form the two sub bboxes.
	MayaBVHAxis split = MayaBVHAxis_Unknown;
	double bboxDepth = bbox.depth();
	double bboxHeight = bbox.height();
	double bboxWidth = bbox.width();

	MPoint bboxMinPt = bbox.min();
	MPoint bboxMaxPt = bbox.max();
	MPoint bboxSplitPt = MPoint(bboxMaxPt);

	if (bboxDepth > bboxHeight && bboxDepth > bboxWidth) {
		split = MayaBVHAxis_XY;
		bboxSplitPt.z = bboxMinPt.z + ((bboxMaxPt.z - bboxMinPt.z) / 2.0);
	} else if (bboxHeight > bboxDepth && bboxHeight > bboxWidth) {
		split = MayaBVHAxis_XZ;
		bboxSplitPt.y = bboxMinPt.y + ((bboxMaxPt.y - bboxMinPt.y) / 2.0);
	} else if (bboxWidth > bboxDepth && bboxWidth > bboxHeight) {
		split = MayaBVHAxis_YZ;
		bboxSplitPt.z = bboxMinPt.z + ((bboxMaxPt.z - bboxMinPt.z) / 2.0);
	} else {
		// NOTE: (sonictk) If the bounding box is a unit cube, we default to one axis.
		split = MayaBVHAxis_XY;
	}

	MBoundingBox bboxA = MBoundingBox(bboxMinPt, bboxSplitPt);
	MBoundingBox bboxB = MBoundingBox(bboxSplitPt, bboxMaxPt);

	// NOTE: (sonictk) Now find which objects lie across which half and split into the sub-arrays.
	for (unsigned int i=0; i < numNodes; ++i) {
		MObject curNode = nodes[i];
		if (curNode.isNull()) {
			continue;
		}

		if (curNode.hasFn(MFn::kTransform)) {
			MFnTransform fnTransform;
			MStatus stat = fnTransform.setObject(curNode);
			if (stat != MStatus::kSuccess) { continue; }
			MDagPath curDagPath;
			stat = fnTransform.getPath(curDagPath);
			if (stat != MStatus::kSuccess) { continue; }
			fnTransform.setObject(curDagPath);
			MVector curPos = fnTransform.getTranslation(MSpace::kWorld);
			if (bboxA.contains(MPoint(curPos))) {
				a.append(curNode);
			} else {
				b.append(curNode); // NOTE: (sonictk) It's got to be in one of either sub bboxes
			}
		} else if (curNode.hasFn(MFn::kMesh)) {


		} else {
			// NOTE: (sonictk) If they are neither a mesh nor transform, can't really
			// partition them spatially.
			continue;
		}
	}

	return;
}


void constructMayaBVHForMObjects(vector<MayaSceneBVHNode>&bvhNodes, const MObjectArray &nodes, const unsigned int minObjsPerLeaf)
{
	// TODO: (sonictk) something with wrong here, stack overflow with more than 1 obj
	MayaSceneBVHNode bvhNode;
	initMayaSceneBVHNode(bvhNode);
	bvhNode.nodes = nodes;

	// NOTE: (sonictk) If there's only one node left (as might be the case for odd-numbers),
	// we can't partition into 2 nodes.
	unsigned int numNodes = nodes.length();
	if (numNodes == 1) {
		bvhNode.bbox = calcBBoxOfMObject(nodes[0]);
	} else {
		bvhNode.bbox = calcBBoxOfMObjects(nodes);
	}

	bvhNodes.push_back(bvhNode);
	if (numNodes <= minObjsPerLeaf) {
		bvhNode.isLeaf = true;

		return;
	}

	bvhNode.isLeaf = false;

	MObjectArray a;
	MObjectArray b;
	partitionMObjectsForBVH(nodes, bvhNode.bbox, a, b);

	size_t numBvhNodes = bvhNodes.size();
	bvhNode.childAIdx = (int)numBvhNodes;
	constructMayaBVHForMObjects(bvhNodes, a, minObjsPerLeaf);

	numBvhNodes = bvhNodes.size();
	bvhNode.childBIdx = (int)numBvhNodes + 1;
	constructMayaBVHForMObjects(bvhNodes, b, minObjsPerLeaf);

	return;
}


MayaBVHStatus constructMayaBVH(vector<MayaSceneBVHNode>&bvhNodes, const int minObjsPerLeaf)
{
	// NOTE: (yliangsiew) Using spatial median partitioning to construct the BVH.
	// This will be top-down construction. Building the tree needs to be fast because we
	// could potentially be re-constructing this across multiple frames.

	// NOTE: (yliangsiew) The reason we can't just filter to meshes is because GPU caches are
	// used heavily in the scenes, and they are not meshes either; so we can't have them
	// be meshes.

	// NOTE: (sonictk) First we get the bbox of the scene to construct the root node for.
	MObjectArray nodes;
	MBoundingBox sceneVolume = calcMayaSceneBBox(nodes);
	unsigned int numObjs = nodes.length();
	switch (numObjs) {
	case 0:
		return MayaBVHStatus_NoNodes;
	case 1:
	{
		MayaSceneBVHNode root;
		initMayaSceneBVHNode(root);
		root.nodes = nodes;
		root.bbox = sceneVolume;

		bvhNodes.push_back(root);

		return MayaBVHStatus_Success;
	}
	default:
		break;
	}

	constructMayaBVHForMObjects(bvhNodes, nodes, minObjsPerLeaf);

	return MayaBVHStatus_Success;
}
