@echo off
REM    This is the Windows build script for the Maya Camera Frustum tools.
REM    usage: build.bat [debug|release|relwithdebinfo|clean] [2012|2015|2017|2019]

setlocal

echo Build script started executing at %time% ...
REM Process command line arguments. Default is to build in release configuration.
set BuildType=%1
if "%BuildType%"=="" (set BuildType=release)

set ProjectName=mayaCamFrustumTools

set MSVCVersion=%2
if "%MSVCVersion%"=="" (set MSVCVersion=2017)

echo Building %ProjectName% in %BuildType% configuration using MSVC %MSVCVersion%...

REM    Set up the Visual Studio environment variables for calling the MSVC compiler;
REM    we do this after the call to pushd so that the top directory on the stack
REM    is saved correctly; the check for DevEnvDir is to make sure the vcvarsall.bat
REM    is only called once per-session (since repeated invocations will screw up
REM    the environment)

if defined DevEnvDir goto build_setup

if %MSVCVersion%==2015 (
   call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
   goto build_setup

)

if %MSVCVersion%==2017 (
   call "%vs2017installdir%\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

if %MSVCVersion%==2019 (
   call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

echo Invalid MSVC compiler version specified.
goto error

:build_setup


REM    Make a build directory to store artifacts; remember, %~dp0 is just a special
REM    FOR variable reference in Windows that specifies the current directory the
REM    batch script is being run in
set BuildDir=%~dp0msbuild

if "%BuildType%"=="clean" (
    REM This allows execution of expressions at execution time instead of parse time, for user input
    setlocal EnableDelayedExpansion
    echo Cleaning build from directory: %BuildDir%. Files will be deleted^^!
    echo Continue ^(Y/N^)^?
    set /p ConfirmCleanBuild=
    if "!ConfirmCleanBuild!"=="Y" (
        echo Removing files in %BuildDir%...
        del /s /q %BuildDir%\*.*
    )
    goto end
)


echo Building in directory: %BuildDir%

if not exist %BuildDir% mkdir %BuildDir%

if %errorlevel% neq 0 goto error

pushd %BuildDir%


REM    Set up globals
set MayaRootDir=C:\Program Files\Autodesk\Maya2020
set MayaIncludeDir=%MayaRootDir%\include
set MayaLibraryDir=%MayaRootDir%\lib
set MayaPluginEntryPoint=%~dp0src\%ProjectName%_plugin_main.cpp

set QtMocPath=%MayaRootDir%\bin\moc.exe
set QtRccPath=%MayaRootDir%\bin\rcc.exe
set QtMocEntryPoint=%~dp0src\%ProjectName%_qt_gui.h
set QtMocPreprocessedSource=%BuildDir%\moc_%ProjectName%_qt_gui.cpp
set QtMocIntermediateObj=%BuildDir%\moc_%ProjectName%_qt_gui.obj


REM    We pipe errors to null, since we don't care if it fails
del *.pdb > NUL 2> NUL


REM    Setup all the compiler flags
set CommonCompilerFlags=/c /MP /W4 /WX /Gy /Zc:wchar_t /Zc:forScope /Zc:inline /openmp /fp:precise /nologo /EHsc /MD /D REQUIRE_IOSTREAM /D _CRT_SECURE_NO_WARNINGS /D _BOOL /D NT_PLUGIN /D _WINDLL /D _MBCS /Gm- /GS /Gy /Gd /TP

REM    Add the include directories for header files
set CommonCompilerFlags=%CommonCompilerFlags% /I"%MayaRootDir%\include"

set CommonCompilerFlagsDebug=/Zi /Od %CommonCompilerFlags%
set CommonCompilerFlagsRelease=/O2 %CommonCompilerFlags%

set MayaPluginCompilerFlagsDebug=%CommonCompilerFlagsDebug% %MayaPluginEntryPoint% /Fo"%BuildDir%\%ProjectName%.obj"
set MayaPluginCompilerFlagsRelease=%CommonCompilerFlagsRelease% %MayaPluginEntryPoint% /Fo"%BuildDir%\%ProjectName%.obj"


REM    Setup all the linker flags
set CommonLinkerFlags=/nologo /incremental:no /manifestuac:"level='asInvoker' uiAccess='false'" /manifest:embed /subsystem:console /tlbid:1 /dynamicbase /nxcompat /machine:x64 /dll

REM    Now add the OS libraries to link against
set CommonLinkerFlags=%CommonLinkerFlags% /pdb:"%BuildDir%\%ProjectName%.pdb" /implib:"%BuildDir%\%ProjectName%.lib"
set CommonLinkerFlags=%CommonLinkerFlags% /defaultlib:Shlwapi.lib /defaultlib:Kernel32.lib /defaultlib:user32.lib /defaultlib:gdi32.lib /defaultlib:winspool.lib /defaultlib:Shell32.lib /defaultlib:ole32.lib /defaultlib:oleaut32.lib /defaultlib:uuid.lib /defaultlib:comdlg32.lib /defaultlib:advapi32.lib

REM    Add all the Maya libraries to link against
set CommonLinkerFlags=%CommonLinkerFlags% "%MayaLibraryDir%\OpenMaya.lib" "%MayaLibraryDir%\OpenMayaAnim.lib" "%MayaLibraryDir%\OpenMayaFX.lib" "%MayaLibraryDir%\OpenMayaRender.lib" "%MayaLibraryDir%\OpenMayaUI.lib" "%MayaLibraryDir%\Foundation.lib" "%MayaLibraryDir%\clew.lib" "%MayaLibraryDir%\Image.lib"
REM    Add all the QT libraries to link against
set CommonLinkerFlags=%CommonLinkerFlags% "%MayaLibraryDir%\Qt5Core.lib" "%MayaLibraryDir%\Qt5Gui.lib" "%MayaLibraryDir%\Qt5Widgets.lib"

set CommonLinkerFlagsDebug=/debug /opt:noref %CommonLinkerFlags%
set CommonLinkerFlagsRelease=/opt:ref %CommonLinkerFlags%

set MayaPluginExtension=mll

set MayaPluginLinkerFlagsCommon=/export:initializePlugin /export:uninitializePlugin /out:"%BuildDir%\%ProjectName%.%MayaPluginExtension%"
set MayaPluginLinkerFlagsRelease=%MayaPluginLinkerFlagsCommon% %CommonLinkerFlagsRelease% "%QtMocIntermediateObj%" "%BuildDir%\%ProjectName%.obj"
set MayaPluginLinkerFlagsDebug=%MayaPluginLinkerFlagsCommon% %CommonLinkerFlagsDebug% "%QtMocIntermediateObj%" "%BuildDir%\%ProjectName%.obj"


echo Running Qt Meta-object compiler (command follows)...
echo "%QtMocPath%" -I "%MayaIncludeDir%" "%QtMocEntryPoint%" -o "%QtMocPreprocessedSource%"
"%QtMocPath%" "%QtMocEntryPoint%" -o "%QtMocPreprocessedSource%"
if %errorlevel% neq 0 goto error


set QtMocCompilerFlagsRelease=%CommonCompilerFlagsRelease% /Fo"%QtMocIntermediateObj%"
set QtMocCompilerFlagsDebug=%CommonCompilerFlagsDebug% /Fo"%QtMocIntermediateObj%"


if "%BuildType%"=="debug" (
    echo Building in debug mode...

    set QtMocCompilerFlags=%QtMocCompilerFlagsDebug% "%QtMocPreprocessedSource%"
    set MayaPluginCompilerFlags=%MayaPluginCompilerFlagsDebug%
    set MayaPluginLinkerFlags=%MayaPluginLinkerFlagsDebug%

) else (
    echo Building in release mode...

    set QtMocCompilerFlags=%QtMocCompilerFlagsRelease%
    set MayaPluginCompilerFlags=%MayaPluginCompilerFlagsRelease%
    set MayaPluginLinkerFlags=%MayaPluginLinkerFlagsRelease%
)


echo Compiling Qt preprocessed sources (command follows)...
echo cl %QtMocCompilerFlags%
cl %QtMocCompilerFlags%
if %errorlevel% neq 0 goto error


REM Now build the Maya plugin
echo Compiling Maya plugin (command follows)...
echo cl %MayaPluginCompilerFlags%
cl %MayaPluginCompilerFlags%
if %errorlevel% neq 0 goto error


:link
echo Linking (command follows)...
echo link %MayaPluginLinkerFlags%
link %MayaPluginLinkerFlags%
if %errorlevel% neq 0 goto error
if %errorlevel% == 0 goto success


:error
echo ***************************************
echo *      !!! An error occurred!!!       *
echo ***************************************
goto end


:success
echo ***************************************
echo *    Build completed successfully!    *
echo ***************************************
goto end


:end
echo Build script finished execution at %time%.
popd
exit /b %errorlevel%
